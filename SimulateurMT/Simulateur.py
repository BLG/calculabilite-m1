#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

# Blanc
B = 'B'

# Mettre l'état initial en premier dans états
class Machine:
    def __init__(self, alphabet, etats, finals, delta):
        self.alphabet = alphabet
        self.etats = etats
        self.finals = finals
        self.delta = delta
        self.Etats = self.etats + self.finals
        self.etat2num = dict()
        for i in range(len(self.Etats)):
            self.etat2num[self.Etats[i]] = i
        
    def table_tex(self):
        sortie = ['\\begin{tabular}{c|%s}' % (len(self.etats)*'c'), ' '.join(['& $%s$'%q for q in self.etats])+' \\\\','\\hline']
        for a in self.alphabet:
            ligne = ['$%s$' % a]
            for q in self.etats:
                ligne.append('&')
                if (q,a) in self.delta:
                    trans = self.delta[(q,a)]
                    ligne.append('$%s,%s,%s$' % (trans[0],trans[1],'\\rightarrow' if trans[2]=='>' else '\\leftarrow' if trans[2]=='<' else '\\downarrow'))
            ligne.append('\\\\')
            sortie.append(' '.join(ligne))
        sortie.append('\\end{tabular}')
        return '\n'.join(sortie)

    def graphe_dot(self):
        sortie = ['digraph G {','q%d [label="%s",shape=square];' % (self.etat2num[self.etats[0]],self.etats[0])]
        for q in self.etats[1:]:
            sortie.append('q%d [label="%s",shape=circle];' % (self.etat2num[q],q))
        for q in self.finals:
            sortie.append('q%d [label="%s",shape=doublecircle];' % (self.etat2num[q],q))
        for (q,a) in self.delta:
            (r,b,d) = self.delta[(q,a)]
            sortie.append('q%d -> q%d [label="%s|%s,%s"];' % (self.etat2num[q],self.etat2num[r],a,b,d))
        sortie.append('}')
        return '\n'.join(sortie)
    
    def graphe_tikz(self):
        sortie = ['\\begin{tikzpicture}','\\tikzstyle{nosty}=[fill=black!20, line width=0.5mm, draw=black!80, circle, minimum width=0.8cm]','\\tikzstyle{flsty}=[->, >=stealth\', thick]']
        for q in self.etats:
            sortie.append('\\node[nosty] (N%d) at (%d,%d) {$%s$};' % (self.etat2num[q],2*self.etat2num[q],2*self.etat2num[q],q))
        for q in self.finals:
            sortie.append('\\node[nosty,double] (N%d) at (%d,%d) {$%s$};' % (self.etat2num[q],2*self.etat2num[q],2*self.etat2num[q],q))
        for (q,a) in self.delta:
            (r,b,d) = self.delta[(q,a)]
            sortie.append('\\draw[flsty] (N%d) edge node[above]{$%s|%s,%s$} (N%d);' % (self.etat2num[q],a,b,'\\rightarrow' if d=='>' else '\\leftarrow' if d=='<' else '\\downarrow',self.etat2num[r]))
        sortie.append('\\end{tikzpicture}')
        return '\n'.join(sortie)

    def sortie_simulateur(self, init):
        sortie = [init,'',self.etats[0],' '.join(self.finals),' '.join(self.finals),'']
        for (q,a) in self.delta:
            (r,b,d) = self.delta[(q,a)]
            sortie.append('%s %s : %s %s %s' % (q,('_' if a==B else a),r,('_' if b==B else b),('R' if d=='>' else 'L' if d=='<' else 'N')))
        return '\n'.join(sortie)

    def simule(self, espace, temps, entree, dx):
        bande = [B for i in range(espace)]
        for i in range(len(entree)):
            bande[dx+i] = entree[i]
        p = dx
        q = self.etats[0]
        sortie = ['\\begin{supertabular}{|%s}\n\\hline' % (espace*'c|')]
        def affiche_bande():
            f = lambda a: ' ' if a==B else a
            sortie.append(' & '.join(['\\etat{$%s$} $%s$'%(q,f(bande[i])) if i==p else '$%s$'%f(bande[i]) for i in range(espace)])+' \\\\')
            sortie.append('\\hline')
        affiche_bande()
        for t in range(temps):
            a = bande[p]
            if (q,a) in self.delta:
                (q,bande[p],d) = self.delta[(q,a)]
                p += -1 if d=='<' else 1 if d=='>' else 0
                affiche_bande()
                if q in self.finals:
                    print >> sys.stderr, 'État final atteint :', q
                    break
                if p<0 or p>=espace:
                    print >> sys.stderr, 'Sortie de la bande : position', p
                    break
            else:
                print >> sys.stderr, 'Transition non définie : (%s,%s)'%(q,a)
                break
        sortie.append('\\end{supertabular}')
        return '\n'.join(sortie)


def usage():
    print >> sys.stderr, './Simulateur.py [code_machine]'
    sys.exit(1)


def main():
    if len(sys.argv)!=2:
        usage()
    if sys.argv[1]=='jouet':
        m = Machine(['0','1','B'],['i'],['f'],{('i','0'):('i','1','>'),('i','1'):('i','0','>'),('i','B'):('f','B','v')})
        open('table.tex','w').write(m.table_tex())
        open('graphe.dot','w').write(m.graphe_dot())
        open('diagramme.tex','w').write(m.simule(8,10,'01010',1))
    elif sys.argv[1]=='anbn':
        m = Machine(['a','b','B'],['e_a','d_d','e_b','d_g'],['O','N'],{('e_a','a',):('d_d','B','>'),('e_a','b'):('N','b','v'),('e_a','B'):('O','B','v'),('d_d','a'):('d_d','a','>'),('d_d','b'):('d_d','b','>'),('d_d','B'):('e_b','B','<'),('e_b','b'):('d_g','B','<'),('e_b','a'):('N','a','v'),('e_b','B'):('N','B','v'),('d_g','a'):('d_g','a','<'),('d_g','b'):('d_g','b','<'),('d_g','B'):('e_a','B','>')})
        open('table.tex','w').write(m.table_tex())
        open('graphe.dot','w').write(m.graphe_dot())
        open('diagramme.tex','w').write(m.simule(7,30,'aaabbb',0))
        open('diagramme2.tex','w').write(m.simule(6,30,'aaabb',0))
        open('diagramme3.tex','w').write(m.simule(6,30,'aabab',0))
    elif sys.argv[1]=='miroir':
        m = Machine(['a','b',"a'","b'",'B'],['i','d_a','d_b','g_a','g_b','e_a','e_b'],['H'],{('i','a'):('d_a',"a'",'>'),('i','b'):('d_b',"b'",'>'),('i','B'):('H','B','v'),('i',"a'"):('H',"a'",'v'),('i',"b'"):('H',"b'",'v'),('d_a','a'):('d_a','a','>'),('d_a','b'):('d_a','b','>'),('d_a','B'):('e_a','B','<'),('d_a',"a'"):('e_a',"a'",'<'),('d_a',"b'"):('e_a',"b'",'<'),('d_b','a'):('d_b','a','>'),('d_b','b'):('d_b','b','>'),('d_b','B'):('e_b','B','<'),('d_b',"a'"):('e_b',"a'",'<'),('d_b',"b'"):('e_b',"b'",'<'),('e_a','a'):('g_a',"a'",'<'),('e_a','b'):('g_b',"a'",'<'),('e_b','a'):('g_a',"b'",'<'),('e_b','b'):('g_b',"b'",'<'),('g_a','a'):('g_a','a','<'),('g_a','b'):('g_a','b','<'),('g_a',"a'"):('i',"a'",'>'),('g_a',"b'"):('i',"a'",'>'),('g_b','a'):('g_b','a','<'),('g_b','b'):('g_b','b','<'),('g_b',"a'"):('i',"b'",'>'),('g_b',"b'"):('i',"b'",'>'),('e_a',"a'"):('H',"a'",'v'),('e_b',"b'"):('H',"b'",'v')})
        open('table.tex','w').write(m.table_tex())
        open('graphe.dot','w').write(m.graphe_dot())
        open('diagramme.tex','w').write(m.simule(7,30,'aababa',0))
    elif sys.argv[1]=='incr':
        m = Machine(['0','1','B'],['i','d','z'],['H'],{('i','B'):('H','B','v'),('i','0'):('d','0','>'),('i','1'):('d','1','>'),('d','0'):('d','0','>'),('d','1'):('d','1','>'),('d','B'):('z','B','<'),('z','1'):('z','0','<'),('z','0'):('H','1','v'),('z','B'):('H','1','v')})
        open('table.tex','w').write(m.table_tex())
        open('graphe.dot','w').write(m.graphe_dot())
        open('diagramme.tex','w').write(m.simule(7,30,'110111',0))
        open('diagramme1.tex','w').write(m.simule(7,30,'11111',1))
    elif sys.argv[1]=='puiss2':
        m = Machine(['1','x','B'],['c_0','c_1','r_0','r_1','g'],['O','N'],{('c_0','1'):('c_1','1','>'),('c_0','x'):('c_0','x','>'),('c_0','B'):('N','B','v'),('c_1','1'):('r_0','x','>'),('c_1','x'):('c_1','x','>'),('c_1','B'):('O','B','v'),('r_0','x'):('r_0','x','>'),('r_0','1'):('r_1','1','>'),('r_1','x'):('r_1','x','>'),('r_1','1'):('r_0','x','>'),('r_0','B'):('g','B','<'),('r_1','B'):('N','B','v'),('g','1'):('g','1','<'),('g','x'):('g','x','<'),('g','B'):('c_0','B','>')})
        open('table.tex','w').write(m.table_tex())
        open('graphe.dot','w').write(m.graphe_dot())
        open('diagramme.tex','w').write(m.simule(6,50,'1111',1))
        open('diagramme1.tex','w').write(m.simule(8,50,'111111',1))
        open('diagramme2.tex','w').write(m.simule(10,100,'11111111',1))
    elif sys.argv[1]=='miroir2':
        m = Machine(['a','b','x','B'],['i','d','f','l','l_a','l_b','e_a','e_b','g','F'],['H'],{('i','B'):('H','B','v'),('i','a'):('d','a','<'),('i','b'):('d','b','<'),('d','B'):('f','x','>'),('f','a'):('f','a','>'),('f','b'):('f','b','>'),('f','B'):('l','x','<'),('l','a'):('l_a','B','>'),('l','b'):('l_b','B','>'),('l_a','B'):('l_a','B','>'),('l_b','B'):('l_b','B','>'),('l_a','x'):('e_a','x','>'),('l_b','x'):('e_b','x','>'),('e_a','a'):('e_a','a','>'),('e_a','b'):('e_a','b','>'),('e_a','B'):('g','a','<'),('e_b','a'):('e_b','a','>'),('e_b','b'):('e_b','b','>'),('e_b','B'):('g','b','<'),('g','x'):('l','x','<'),('g','a'):('g','a','<'),('g','b'):('g','b','<'),('l','B'):('l','B','<'),('l','x'):('F','B','>'),('F','B'):('F','B','>'),('F','x'):('H','B','>')})
        open('table.tex','w').write(m.table_tex())
        open('graphe.dot','w').write(m.graphe_dot())
        open('diagramme.tex','w').write(m.simule(8,100,'aab',1))
    elif sys.argv[1]=='diff':
        m = Machine(['1','x','B'],['i','d','f_1','f_2','m','z_g','z_d','r','e'],['H'],{('i','1'):('d','1','<'),('i',B):('d',B,'<'),('d',B):('f_1','x','>'),('f_1','1'):('f_1','1','>'),('f_1',B):('f_2',B,'>'),('f_2','1'):('f_2','1','>'),('f_2',B):('m','x','<'),('m','1'):('m','1','<'),('m',B):('z_d','B','>'),('z_d',B):('z_d',B,'>'),('z_d','1'):('z_g',B,'<'),('z_g',B):('z_g',B,'<'),('z_g','1'):('z_d',B,'>'),('z_d','x'):('r',B,'<'),('z_g','x'):('e',B,'>'),('e','x'):('H',B,'v'),('e','1'):('e',B,'>'),('e',B):('e',B,'>'),('r',B):('r',B,'<'),('r','1'):('r','1','<'),('r','x'):('H',B,'>')})
        open('table.tex','w').write(m.table_tex())
        open('graphe.dot','w').write(m.graphe_dot())
        open('diagramme.tex','w').write(m.simule(8,100,'111B11',1))
        open('diagramme1.tex','w').write(m.simule(7,100,'1B111',1))
        #open('diagramme.tex','w').write(m.simule(8,100,'111B',1))
        #open('diagramme.tex','w').write(m.simule(8,100,'111B',1))
        #print m.sortie_simulateur('111_11')
    elif sys.argv[1]=='anb2np1':
        m = Machine(['a','b','B'],['e_a','d_d','e_b','e_b^2','d_g','f'],['O','N'],{('e_a','a'):('d_d','B','>'),('e_a','b'):('f','b','>'),('e_a','B'):('N','B','v'),('f',B):('O',B,'v'),('f','a'):('N','a','v'),('f','b'):('N','b','v'),('d_d','a'):('d_d','a','>'),('d_d','b'):('d_d','b','>'),('d_d','B'):('e_b','B','<'),('e_b','b'):('e_b^2','B','<'),('e_b','a'):('N','a','v'),('e_b','B'):('N','B','v'),('e_b^2','b'):('d_g','B','<'),('e_b^2','a'):('N','a','v'),('e_b^2','B'):('N','B','v'),('d_g','a'):('d_g','a','<'),('d_g','b'):('d_g','b','<'),('d_g','B'):('e_a','B','>')})
        open('table.tex','w').write(m.table_tex())
        open('graphe.dot','w').write(m.graphe_dot())
        open('diagramme.tex','w').write(m.simule(8,30,'aabbbbb',0))
        open('graphe.tex','w').write(m.graphe_tikz())
    elif sys.argv[1]=='gray':
        m = Machine(['0','1','B'],['a','b','c','d'],['F'],{('a','0'):('a','0','>'),('a','1'):('b','1','>'),('b','0'):('b','0','>'),('b','1'):('a','1','>'),('a','B'):('c','B','<'),('c','0'):('F','1','v'),('c','B'):('F','1','v'),('c','1'):('F','0','v'),('b','B'):('d','B','<'),('d','0'):('d','0','<'),('d','1'):('c','1','<')})
        open('table.tex','w').write(m.table_tex())
        open('graphe.dot','w').write(m.graphe_dot())
        open('diagramme.tex','w').write(m.simule(9,30,'10110000',0))
        #open('diagramme.tex','w').write(m.simule(9,30,'11010100',0))
        open('graphe.tex','w').write(m.graphe_tikz())
    elif sys.argv[1]=='puiss3_faux':
        # Code CC 2015
        m = Machine(['1','x','B'],['a','b','c','g'],['N'],{('a','1'):('b','1','>'),('a','x'):('a','x','>'),('a','B'):('g','B','<'),('b','1'):('c','x','>'),('b','x'):('b','x','>'),('b','B'):('N','B','v'),('c','1'):('a','x','>'),('c','x'):('c','x','>'),('c','B'):('N','B','v'),('g','1'):('g','1','<'),('g','x'):('g','x','<'),('g','B'):('a','B','>')})
        # Code etudiant : multiples de 9
        #m = Machine(['1','x','B'],['a','a_2','b','c','g'],['O','N'],{('a','1'):('b','1','>'),('a','x'):('a_2','x','>'),('a','B'):('g','B','<'),('a_2','1'):('b','1','>'),('a_2','x'):('a_2','x','>'),('a_2','B'):('O','B','v'),('b','1'):('c','x','>'),('b','x'):('b','x','>'),('b','B'):('N','B','v'),('c','1'):('a','x','>'),('c','x'):('c','x','>'),('c','B'):('N','B','v'),('g','1'):('g','1','<'),('g','x'):('g','x','<'),('g','B'):('a','B','>')})
        open('table.tex','w').write(m.table_tex())
        open('graphe.dot','w').write(m.graphe_dot())
        #open('diagramme.tex','w').write(m.simule(5,55,'111',1))
        #open('diagramme.tex','w').write(m.simule(8,55,'111111',1))
        #open('diagramme.tex','w').write(m.simule(9,55,'1111111',1))
        open('diagramme.tex','w').write(m.simule(11,100,'111111111',1))
        #open('diagramme.tex','w').write(m.simule(20,200,18*'1',1))
        #open('diagramme.tex','w').write(m.simule(29,200,27*'1',1))
        open('graphe.tex','w').write(m.graphe_tikz())
        #for i in range(1,300):
        #    m.simule(i+2,5000,i*'1',1)
    elif sys.argv[1]=='puiss3_sol1':
        m = Machine(['1','x','B'],['a_0','b_0','a','b','c','g'],['O','N'],{('a_0','1'):('b_0','1','>'),('a_0','x'):('a_0','x','>'),('a_0','B'):('N','B','v'),('a','1'):('b','1','>'),('a','x'):('a','x','>'),('a','B'):('g','B','<'),('b_0','1'):('c','x','>'),('b_0','x'):('b_0','x','>'),('b_0','B'):('O','B','v'),('b','1'):('c','x','>'),('b','x'):('b','x','>'),('b','B'):('N','B','v'),('c','1'):('a','x','>'),('c','x'):('c','x','>'),('c','B'):('N','B','v'),('g','1'):('g','1','<'),('g','x'):('g','x','<'),('g','B'):('a_0','B','>')})
        open('table.tex','w').write(m.table_tex())
        open('graphe.dot','w').write(m.graphe_dot())
        open('diagramme.tex','w').write(m.simule(11,100,'111111111',1))
        open('graphe.tex','w').write(m.graphe_tikz())
        #for i in range(1,300):
        #m.simule(i+2,5000,i*'1',1)
    elif sys.argv[1]=='puiss3_sol2':
        m = Machine(['1','x','B'],['a','b','c','g_0','g_1','g'],['O','N'],{('a','1'):('b','1','>'),('a','x'):('a','x','>'),('a','B'):('g_0','B','<'),('b','1'):('c','x','>'),('b','x'):('b','x','>'),('b','B'):('N','B','v'),('c','1'):('a','x','>'),('c','x'):('c','x','>'),('c','B'):('N','B','v'),('g_0','1'):('g_1','1','<'),('g_0','x'):('g_0','x','<'),('g_0','B'):('N','B','v'),('g_1','1'):('g','1','<'),('g_1','x'):('g_1','x','<'),('g_1','B'):('O','B','v'),('g','1'):('g','1','<'),('g','x'):('g','x','<'),('g','B'):('a','B','>')})
        open('table.tex','w').write(m.table_tex())
        open('graphe.dot','w').write(m.graphe_dot())
        open('diagramme.tex','w').write(m.simule(11,100,'111111111',1))
        open('graphe.tex','w').write(m.graphe_tikz())
        #for i in range(1,300):
        #m.simule(i+2,5000,i*'1',1)
    else:
        usage()

main()
