\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage[hmargin=2cm, vmargin=2.3cm]{geometry}
\usepackage{tgpagella}
\usepackage{amsmath,amssymb}
\usepackage{tikz, xspace, enumerate, url}



%%%%% STYLE %%%%%
 % noindent
\setlength\parindent{0pt}

 % Header/Footer
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\lhead{Calculabilité et Complexité 2012-2013}
\rhead{M1 Info - Université d'Orléans}
%\rfoot{\thepage/\ref{lastpage}}

%\renewcommand{\labelitemi}{\textbullet}


%%%%% MACROS %%%%%
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\renewcommand{\P}{\mathcal{P}}
\newcommand{\BB}{\mathbb{B}}
\newcommand{\ssi}{si et seulement si\xspace}
\newcommand{\ssil}{si et seulement s'il\xspace}
\newcommand{\titre}[1]{\vspace{1cm}\begin{center}\LARGE\bfseries#1\end{center}}
\newcommand{\groupe}[1]{\begin{center}\rule{\linewidth}{0.5pt}\\\large\bfseries#1\\\vspace{-0.2cm}\rule{\linewidth}{0.5pt}\end{center}}
\newcounter{numexo}
\setcounter{numexo}{0}
\newcommand{\exercice}[1]{\addtocounter{numexo}{1}\paragraph{Exercice \thenumexo}#1}
\renewcommand{\geq}{\geqslant}
\renewcommand{\leq}{\leqslant}
\newcommand{\card}[1]{\lvert#1\rvert}

%%%%% DOCUMENT %%%%%
\begin{document}

\titre{TD 1 - Machines de Turing et Codage}

\groupe{Machines de Turing}

Toutes les machines considérées dans ce TD disposent, en plus des
alphabets spécifiés, d'un symbole \emph{blanc} noté $B$ et, sauf
indication contraire explicite, d'un unique ruban.

\exercice{Machine mystère}\\
On considère une machine dont l'ensemble des états est $\{a,b,c,d,e\}$, avec $a$ l'état initial et $e$ l'état final, travaillant sur l'alphabet (de travail) $\{1,B\}$ et dont les transitions sont décrites par la table suivante.
\begin{table}[h]\centering
\begin{tabular}{c|cccc}
&$a$&$b$&$c$&$d$\\
\hline
$B$&$a,B,\rightarrow$&$e,B,\downarrow$&$e,B,\downarrow$&$a,B,\rightarrow$\\
$1$&$b,1,\rightarrow$&$c,1,\rightarrow$&$d,B,\leftarrow$&$d,B,\leftarrow$\\
\end{tabular}
\end{table}
\begin{enumerate}
\item Que fait cette machine depuis l'entrée $11111111$ ? Que semble-t-elle calculer ?
\item La machine ne s'arrête pas sur certaines entrées. Lesquelles ? Corriger la machine pour régler ce problème.
\end{enumerate}

\exercice{Un peu de langages formels}
\begin{enumerate}
\item Programmer une machine de Turing qui reconnaît les mots de la
  forme $a^nb^n$, $n\in\N$, sur l'alphabet $\{a,b\}$. On donnera la
  table de transition complète de la machine.
\item Programmer une machine de Turing qui calcule le miroir d'un mot
  sur l'alphabet $\{a,b\}$. On donnera la représentation complète sous
  forme d'automate de la machine.
\item Programmer une machine de Turing qui reconnaît les mots de la forme $\alpha\overline{\alpha}\alpha$ où $\alpha\in\{a,b\}^*$.
\end{enumerate}

\exercice{Un peu d'arithmétique}
\begin{enumerate}
\item Programmer une machine de Turing qui calcule la \emph{quasi-différence} ($a\circleddash
  b=\max(0,a-b)$) en unaire (l'entrée est donnée sous la forme $\underbrace{11\cdots1}_aB\underbrace{11\cdots1}_b$).
\item Programmer une machine de Turing qui ajoute 1 à un nombre codé
  en binaire sur son entrée.
\item Programmer une machine de Turing qui reconnaît les puissances
  de~2 codées en binaire, puis en unaire.
\end{enumerate}

%\bigbluebox{\textbf{\titlefont Simulation}~~~Formellement, une machine $M'$ sur un alphabet $\Sigma'$ \emph{simule} une machine $M$ sur un alphabet $\Sigma$ s'il existe une fonction locale de codage (i.e. un morphisme alphabétique) $c:\Sigma\rightarrow\Sigma'^*$ tel que pour tout $x\in\Sigma^*$, $c(M(x))=M'(c(x))$.\\Dans la suite, on pourra cependant se contenter d'une notion \emph{intuitive} de simulation.}

\exercice{Variantes usuelles}\\
Montrer que les modèles suivant sont équivalents en termes de
calculabilité au modèle de machine de Turing \emph{classique} à 1~ruban et
dont l'alphabet de travail ne contient que 3~symboles $\{0,1,B\}$, i.e. qu'ils calculent exactement les mêmes fonctions. On montrera pour cela qu'ils peuvent \emph{simuler} et
\emph{être simulés}, en un sens \emph{intuitif}, par des
machines de Turing classiques :
\begin{enumerate}
\item les machines de Turing sur un alphabet de travail $\Sigma$, $\card{\Sigma}\geq2$, quelconque ;
\item les machines de Turing dont la tête est toujours en mouvement (pas de transition pour laquelle la tête reste fixe) ;
\item les machines de Turing dont le ruban n'est infini que d'un côté (i.e. est indexé par $\N$ et non plus $\Z$) ;
\item les machines de Turing à $k\geq2$ rubans.
\end{enumerate}

%\vspace{0.5cm}
\newpage
\groupe{Codage}

Pour $X$ un ensemble quelconque, on note $\P(X)$ l'ensemble des
parties de $X$.\\

Une application $f:E\rightarrow F$ est une \emph{bijection} si elle
établit une correspondance élément-à-élément entre les ensembles $E$
et $F$ : pour tout $y\in F$, \textbf{il existe un unique} $x\in E$ tel que $f(x)=y$. En notant $f^{-1}(y)$ cet unique $x$, on définit une application bijective $f^{-1}:F\rightarrow E$ appelée \emph{inverse} de $f$.\\

Un ensemble est \emph{dénombrable} s'il peut être mis en bijection avec $\N$.

\exercice{Quelques ensembles dénombrables}\\
En informatique, un objet est codable s'il est représentable par un
mot fini de bits. En notant $\BB=\{0,1\}$, l'ensemble des codages est $\BB^*$.
\begin{enumerate}
\item Montrer que les ensembles suivants sont \emph{récursivement}
  dénombrables en exhibant à chaque fois une bijection \emph{calculable} (par
  machine de Turing) avec $\N$ :
\begin{itemize}
\item l'ensemble des nombres pairs ;
\item l'ensemble des nombres carrés ;
\item l'ensemble $\Z$ des entiers relatifs ;
\item l'ensemble $\N^2$ des couples d'entiers naturels ;
\item les ensembles $\N^p$, $p\geq2$, des $p$-uplets d'entiers naturels ;
\item l'ensemble $\N^{(\N)}$ des suites finies d'entiers naturels ;
\item l'ensemble $\BB^*$ des codages ;
\item l'ensemble des machines de Turing.
\end{itemize}
\item Quels types et structures de données de votre langage de programmation préféré sont codables en utilisant les bijections précédentes (ou du moins des variantes/compositions) ?
\item Justifier que l'ensemble des programmes écrits dans votre langage de programmation préféré est dénombrable. Que pensez-vous de l'ensemble de tous les programmes écrits dans tous les langages de programmation imaginables ?\\
\emph{Indication.}~~On pourra montrer au passage qu'une union dénombrable d'ensembles dénombrables est dénombrable.
\end{enumerate}

\exercice{Argument diagonal de Cantor}
\begin{enumerate}
\item Justifier que $\P(\N)$, $\{0,1\}^{\N}$, l'intervalle réel
  $[0,1]$ et $\R$ sont en bijection.
\item Montrer par l'absurde que ces ensembles ne sont pas dénombrables.\\
\emph{Indication.}~~Supposer $\{0,1\}^\N$ dénombrable et construire une suite ne pouvant être dans l'énumération.
\item En déduire qu'il existe des fonctions non calculables.
\end{enumerate}

%%%%% FIN %%%%%
%\label{lastpage}
\end{document}