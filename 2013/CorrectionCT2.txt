Correction non officielle. Les [?] signalent une interprétation de l'énoncé.

-- Calculabilité --

- "Échauffement" -

1. C'est un langage fini donc trivialement r.e. et récursif. Il serait récursif de toute façon, même pour toutes les grilles n^2 x n^2 pour tout n (NP-complet dans ce cas).

2. S'il existait un tel programme, on saurait décider l'égalité de deux fonctions récursives en comparant leur programme "le plus efficace" : deux fonctions admettent le même programme "efficace" ssi elles sont égales. Or cette question est indécidable (cf. TD).

3*. On dispose d'un codage récursif des triplets < ., ., . >. On note pi_i(<x_1,x_2,x_3>) = x_i, pour i dans {1,2,3}, les trois projections récursives associées. Il existe trois fonctions récursives p_i, pour i dans {1,2,3}, telles que phi_{p_i(n)}(x) = pi_i(phi_n(x)). On a alors toujours phi_n(x) = < phi_{p_1(n)}(x), phi_{p_2(n)}(x), phi_{p_3(n)}(x) > et l'on cherche simplement n tel que phi_n(x) = < p_2(n), p_3(n), p_1(n) >. Ça existe par Kleene, d'où le résultat.


- Réductions -

4. Si (M,x) est une instance de ARRET, on fabrique la machine M' qui simule M(x) sur toutes ses entrées. M' est non divergente (en fait, totale, constante égale à M(x)) ssi M(x) s'arrête (et définie nulle part sinon, donc divergente). D'où le résultat ARRET <= NON DIVERGE.

5*. Si M est une instance de NON DIVERGE, on fabrique la machine M' qui ignore son entrée et simule M sur toutes ses entrées en parallèle et s'arrête dès qu'un des calculs simulés termine. Quel que soit x, M'(x) s'arrête ssi M est non divergente, d'où le résultat.

6. ARRET et NON DIVERGE sont équivalents : NON DIVERGE est r.e. (par q. 5) mais non co-r.e (par q. 4). DIVERGE est donc co-r.e. (ce que l'on pouvait aussi facilement observer en faisant tourner "en parallèle" la machine sur toutes les entrées et en rejetant dès qu'un des calculs s'arrête) mais non r.e.


- Analyse de machine de Turing -

On note a* et b* les a et b soulignés. La tête dans l'état q est indiquée par <q> devant la lettre qu'elle lit.

7. Simulation facile.

8. Même réponse que pour CT de première session. Une fois qu'une lettre soulignée a été "traitée", on ne revient plus jamais à sa gauche.

9**. La machine calcule le mot de Fibonacci, point fixe de la substitution a -> ab, b -> a.



-- Complexité --

- Échauffement -

1. C'est du cours.

2. On a alors un algo. poly. pour tout problème de NP en composant la réduction polynomiale au pb NP-complet avec l'algo poly. pour ce problème.

3*. Les mots "il faut" de la "démonstration" sont incorrects. C'est en réalité un "il suffit" : on dispose en effet d'un algo. exp. pour SAT mais cela ne permet en rien d'affirmer que l'on ne peut faire mieux. Ce n'est pas parce que l'on dispose d'une méthode que l'on ne peut améliorer qu'il n'existe pas de méthode complètement différente qui serait plus efficace.


- Réductions -

4. Un certificat est constitué par une distribution de vérité. Il est de taille linéaire en l'entrée, sa vérification est linéaire.

5**. On reprend les mêmes variables + 1 variable supplémentaire X. On remplace alors chaque clause de 3-SAT C_i = a+b+c par une clause de NAE-4-SAT D_i = a+b+c+X. Sens gauche-droite, on garde les mêmes valeurs pour les variables communes et on prend X=0 : comme pour chaque C_i, il y a un des littéraux qui est à 1 et X à 0, on a bien une solution NAE. Réciproque, quelle que soit la valeur de X, on a dans chaque clause D_i un littéral différent de X. Si X=0, on a donc déjà une solution, si X=1, on inverse toutes les valeurs et on a une solution.

6*. Il s'agit de casser chaque clause de 4 littéraux en 2 clauses de 3 littéraux "NAE-équivalentes" : C_i = a+b+c+d est transformée en D_i = a+b+Y_i et D'_i = (-Y_i)+c+d. On ne fait pas l'analyse détaillée...


- NP-complétude -

7. Cadeau.

8. Clair, un certificat est une distribution de vérité (taille linéaire), sa vérification est bien polynomiale.

9*. Pour chaque variable a, on ajoute les 2 inéquations 0 <= a <= 1. Pour chaque clause C_i = a*+b*+c*, on ajoute les 2 inéquations 1 <= E(a*)+E(b*)+E(c*) <= 2 où E(a*)=a si a*=a et E(a*)=1-a si a*=-a.

NB. On pourrait le faire directement avec 3-SAT et ce serait plus simple  : on n'aurait besoin que de la partie "1<=" pour les clauses.
