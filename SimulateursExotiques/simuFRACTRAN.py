#!/usr/bin/env python

import sys
from fractions import Fraction

def val(p,n):
    v = 0
    while n%p==0:
        v += 1
        n /= p
    return v

def step(P,n):
    for f in P:
        m = f*n
        if m.denominator==1:
            return (True,m)
    return (False,n)

def run(P,n):
    while True:
        #print n # affichage trace
        b,n = step(P,n)
        if not b:
            return n

def main():
    if len(sys.argv) not in [2,3]:
        print >> sys.stderr, 'usage:', sys.argv[0], 'input [out_val] < program'
        sys.exit(1)
    o = run(map(Fraction,sys.stdin.read().split()),int(sys.argv[1]))
    print 'output:', o
    if len(sys.argv)==3:
        p = int(sys.argv[2])
        print 'val_%d(%d) = %d' % (p,o,val(p,o))

main()
