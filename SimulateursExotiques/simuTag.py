#!/usr/bin/env python

class TagSystem:
    def __init__(self,k,R):
        self.del_num = k
        self.rules = R

    def step(self,w):
        if len(w)>=self.del_num:
            return (True,w[self.del_num:]+self.rules[w[0]])
        return (False,w)

    def run(self,w,aff=False):
        while True:
            if aff:
                print w
            b,w = self.step(w)
            if not b:
                return w

    def run_mem(self,w,aff=False):
        S = set([w])
        while True:
            if aff:
                print w
            b,w = self.step(w)
            if (not b) or (w in S):
                return w
            S.add(w)

Pair = TagSystem(2,{'a':'ba','b':''})
Impair = TagSystem(2,{'a':'ab','b':''})
Puiss2 = TagSystem(2,{'a':'ba','b':'a'})
Puiss3 = TagSystem(3,{'a':'baa','b':'a'})
Syracuse = TagSystem(2,{'a':'bc','b':'a','c':'aaa'})

def test_unaire(TS,n):
    l = []
    for i in range(2,n+1):
        if len(TS.run_mem(i*'a'))<TS.del_num:
            l.append(i)
    return l

def main():
    #Puiss3.run_mem('aaaaaaaaa',True)
    print test_unaire(Puiss3,1000)

main()

