#!/usr/bin/env python

import sys

def fibolu(k,n):
    t = [1 for i in range(k)]
    u = 1
    for i in range(n-k+1):
        u = sum(t)
        t.append(u)
        t = t[1:]
        print t
    return u

print fibolu(int(sys.argv[1]),int(sys.argv[2]))
