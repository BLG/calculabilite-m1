#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import sys

class RAM:
    def __init__(self,P):
        self.INSTRS = {'UNIT':(0,1),'ADD':(1,3),'SUB':(2,3),'SHIFT':(3,1),'LOAD':(4,2),'STORE':(5,2),'JNZ':(6,2),'HALT':(7,0)}
        self.i = 0
        self.Reg = {}
        self.Prog = self.parse(P)

    def parse(self,P):
        prog = []
        for l in P:
            instr = l.split()
            if not instr[0] in self.INSTRS:
                print >> sys.stderr, 'Erreur de parsing : instruction', instr[0], 'inconnue !'
                sys.exit(1)
            code,ops = self.INSTRS[instr[0]]
            if len(instr)!=ops+1:
                print >> sys.stderr, "Erreur de parsing : l'instruction %s attend %d opérandes et non %d !" % (instr[0],ops,len(instr)-1)
                sys.exit(1)
            prog.append(tuple([code]+map(int,instr[1:])))
        return prog

    def verifReg(self,r):
        if not r in self.Reg:
            self.Reg[r] = 0

    def step(self):
        if self.i>=len(self.Prog):
            return False
        curr = self.Prog[self.i]
        self.i += 1
        if curr[0]==0:
            self.Reg[curr[1]] = 1
        elif curr[0]==1:
            self.verifReg(curr[2])
            self.verifReg(curr[3])
            self.Reg[curr[1]] = self.Reg[curr[2]]+self.Reg[curr[3]]
        elif curr[0]==2:
            self.verifReg(curr[2])
            self.verifReg(curr[3])
            self.Reg[curr[1]] = max(0,self.Reg[curr[2]]-self.Reg[curr[3]])
        elif curr[0]==3:
            self.verifReg(curr[1])
            self.Reg[curr[1]] /= 2
        elif curr[0]==4:
            self.verifReg(curr[2])
            self.verifReg(self.Reg[curr[2]])
            self.Reg[curr[1]] = self.Reg[self.Reg[curr[2]]]
        elif curr[0]==5:
            self.verifReg(curr[1])
            self.verifReg(curr[2])
            self.Reg[self.Reg[curr[1]]] = self.Reg[curr[2]]
        elif curr[0]==6:
            self.verifReg(curr[1])
            if self.Reg[curr[1]]>0:
                if curr[2]>=len(self.Prog):
                    return False
                self.i = curr[2]
        elif curr[0]==7:
            return False
        return True

    def initReg(self,args):
        for i in range(len(args)):
            self.Reg[i+1] = args[i]
    
    def run(self):
        self.i = 0
        while self.step():
            pass

    def printRun(self):
        self.i = 0
        print 'entrée', self.Reg
        c = 0
        while self.step():
            print 'étape', c, self.Reg
            c += 1
        print 'fin', self.Reg
        self.verifReg(0)
        print 'sortie', self.Reg[0]
        
    def printReg(self):
        print self.Reg

def main():
    if len(sys.argv)==1:
        print >> sys.stderr, 'usage:', sys.argv[0], 'in1 [in2 in3 ...] < prog.RAM'
        sys.exit(1)
    ram = RAM(sys.stdin.readlines())
    ram.initReg(map(int,sys.argv[1:]))
    ram.printRun()

main()
