%% Compiler avec XeLaTeX !
\documentclass[a4paper,10pt]{article}
\usepackage[francais]{babel}
\usepackage[hmargin=1.8cm, vmargin=2cm]{geometry}
%% BUG : conflit entre amssymb et xunicode
%% Solution : les déclarer dans cet ordre
\usepackage{amsmath,amssymb}
\usepackage{fontspec,xunicode}
\usepackage{sfmath} % police maths sans serif
\usepackage{tikz, xspace, enumerate, url}
\usetikzlibrary{arrows,automata}


%%%%% STYLE %%%%%
 % noindent
\setlength\parindent{0pt}

% Polices
\setromanfont{PT Sans}
\setmonofont{PT Mono}
\newcommand{\titlefont}{\fontspec{PT Sans Caption}}

 % Header/Footer
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\lhead{Calculabilité et Complexité 2014-15}
\rhead{M1 Info - Université d'Orléans}
%\rfoot{\thepage/\pageref{lastpage}}


%%%%% MACROS %%%%%
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\titre}[1]{\vspace{1cm}\begin{center}\titlefont\huge\bfseries#1\end{center}}
\newcounter{numexo}
\setcounter{numexo}{0}
\newcommand{\exercice}[2]{\addtocounter{numexo}{1}\paragraph{\titlefont Exercice \thenumexo}#1\hfill\emph{\small#2}\\}
\renewcommand{\geq}{\geqslant}
\renewcommand{\leq}{\leqslant}
\newcommand{\card}[1]{\lvert#1\rvert}
\newcommand{\liste}[1]{\langle#1\rangle}
\renewcommand{\phi}{\varphi}

%% BOITES
\newcommand{\superfantome}{\vphantom{abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ\_}}
\newcommand{\codebox}[1]{\raisebox{-0.135cm}{\tikz{\node[fill=blue!10,rounded corners,inner sep=2pt]{\small\tt\superfantome#1};}}}
%% NB : largeur voulue des grandes box : textwidth + 8pt (4pt de chaque côté)
\newcommand{\groupe}[1]{
  \vspace{0.6cm}%
  \noindent\hspace{-4pt}%
  \tikz{\node[fill=black!70, rounded corners, inner sep=4pt]{\makebox[\textwidth]{\titlefont\color{white}\textbf{\superfantome#1}}};}%
  \vspace{-0.3cm}%
}
%% Correction de la largeur pour inner sep 6pt (au lieu de 4pt)
\newlength{\boxwidth}
\addtolength{\boxwidth}{\textwidth}
\addtolength{\boxwidth}{-4pt}
\newcommand{\bigbox}[2]{\noindent\hspace{-4pt}\tikz{\node[fill=#1,rounded corners,inner sep=6pt]{\parbox{\boxwidth}{#2}};}}
\newcommand{\bigbluebox}[1]{\bigbox{blue!10}{#1}}
\newcommand{\bigredbox}[1]{\bigbox{red!10}{#1}}
\newcommand{\biggreenbox}[1]{\bigbox{green!10}{#1}}
\newcommand{\centercodebox}[1]{
  \begin{center}%
  \tikz{\node[fill=blue!10,rounded corners,inner sep=6pt]{\parbox{0.8\textwidth}{\tt #1}};}%
  \end{center}%
}

%%%%% DOCUMENT %%%%%
\begin{document}

\titre{Partiel de calculabilité}

\begin{center}
\emph{\textbf{Durée : 2h. Documents NON autorisés.\\Le recours à toute
    réalisation effective d'un modèle Turing-complet (calculatrice, portable) est interdite.}}
\end{center}

{\small\emph{Un barème prévisionnel (sur 25 pts) est donné à titre indicatif. Les exercices sont indépendants. Il est recommandé de ne traiter l'exercice~4 qu'après avoir traité le reste du sujet.}}

\bigbluebox{\small On rappelle qu'il est possible de coder les machines de Turing dans les entiers de manière à ce qu'il existe un algorithme effectif de codage/décodage : une machine peut décoder un code pour obtenir la table de transition correspondante et coder une table pour obtenir le code correspondant.\\
Cela permet de définir une énumération (récursive) $(M_n)_{n\geq0}$ des machines de Turing : $M_n$ désigne la machine de code $n$. On note $(\phi_n)_{n\geq0}$ l'énumération des fonctions
calculables associée : $\phi_n:\N\rightarrow\N$ désigne la fonction partielle calculée par $M_n$, définie sur les entrées sur lequelles la machine s'arrête.\\
On rappelle l'existence d'une machine universelle $U$ qui sur l'entrée $(n,x)$ simule la machine $M_n$  sur l'entrée $x$ : $U(n,x)=M_n(x)$.}


\groupe{Machines de Turing}

\exercice{Machine mystère incomplète}{7 pts} On définit une machine de Turing sur l'alphabet $\Sigma=\{1,x,B\}$ (où~$B$ désigne le \emph{blanc}) par la table de transition suivante. L'état initial est~$a$. L'état final est~$N$. Les entrées seront toujours constituées d'un ruban intégralement blanc à l'exception d'un bloc (non vide) de $1$. La tête sera positionnée dans l'état $a$ sur le premier $1$ du bloc.

\begin{center}
\begin{tabular}{c|cccc}
& $a$ & $b$ & $c$ & $g$ \\
\hline
$1$ & $b,1,\rightarrow$ & $c,x,\rightarrow$ & $a,x,\rightarrow$ & $g,1,\leftarrow$ \\
$x$ & $a,x,\rightarrow$ & $b,x,\rightarrow$ & $c,x,\rightarrow$ & $g,x,\leftarrow$ \\
$B$ & $g,B,\leftarrow$ & $N,B,\downarrow$ & $N,B,\downarrow$ & $a,B,\rightarrow$ \\
\end{tabular}
\end{center}

\begin{enumerate}
\item Dessiner le graphe des transitions de cette machine.
\item Simuler cette machine sur les entrées $1^6$, $1^7$ et $1^{18}$.\\{\small Répondre en donnant les diagrammes espace-temps
    des deux premiers calculs et la configuration finale du troisième.}
% (on pourra sauter certaines étapes répétitives à condition d'indiquer clairement par une phrase non ambiguë ce qui se passe dans ces portions non explicitement décrites du diagramme).}
\item Décrire de façon aussi précise et concise que possible ce que fait la machine en fonction de son entrée. À quoi cela peut-il servir (imaginer que l'entrée est un nombre en unaire) ?
\item Quel est le résultat du calcul (configuration finale) à partir des entrées $1^3$, $1^9$ et $1^{27}$ ? En réfléchissant à votre réponse à la question précédente, expliquer pourquoi on préférerait que la machine termine ici dans un nouvel état final \textbf{acceptant} (qu'on appellera $O$, en considérant l'état final actuel $N$ comme \textbf{rejetant}) et décrire le langage qu'elle reconnaîtrait alors.
\item Modifier la machine (on pourra ajouter des états) pour que ce soit le cas.
\end{enumerate}

\groupe{Trois preuves du théorème de l'Arrêt}

\vspace{5mm}

Dans toute cette section, on note $H$ la fonction décidant l'Arrêt des machines de Turing : $H(i,j)=1$ si la machine $M_i$ s'arrête sur l'entrée $j$ et $H(i,j)=0$ sinon. Le \emph{théorème de l'Arrêt} peut alors s'énoncer en ces termes : \emph{$H$ n'est pas calculable}. Nous allons le démontrer de différentes manières dans ce qui suit.

\exercice{Diagonalisation de Cantor}{3 pts}
\vspace{-5mm}
\begin{enumerate}
\item Afin de fixer les intuitions de cette première preuve, supposons que l'on dispose d'une machine $D$ telle que sur toute entrée $n\in\N$, le calcul $D(n)$ s'arrête si et seulement si $H(n,n)=0$. Expliquer pourquoi cette machine est différente de toutes les machines de l'énumération (pour la distinguer de $M_n$ considérer leurs calculs sur l'entrée $n$).
\item On suppose ici $H$ calculable.
\begin{enumerate}
\item Montrer qu'il est alors possible de construire une telle machine $D$.
\item $D$ doit donc apparaître dans l'énumération : il existe $d$ tel que $M_d=D$. Quel est le comportement de $D$ sur son propre code ?
\item Conclure.
\end{enumerate}
\end{enumerate}


\exercice{Castor affairé}{5 pts}
On définit la fonction $\sigma$ par : pour tous $n,m\in\N$, $\sigma(n,m)$ est le nombre maximal de transitions effectuées par une machine à $n$ états (sans compter les états finals) et $m$ symboles (en comptant le blanc) avant son arrêt sur l'entrée vide (c'est-à-dire que l'on ne prend ici en compte que les machines qui s'arrêtent sur l'entrée vide). Les premières questions de cet exercice visent à vérifier que cette définition est bien valide.
\begin{enumerate}
\item Quel est le nombre de machines à $n$ états et $m$ symboles ?\\
{\small Si vous ne parvenez pas à décrire ce nombre en fonction de $n$ et $m$, justifier au moins qu'il est \textbf{fini}.}
\item En déduire que $\sigma$ est \textbf{bien définie}, c'est-à-dire que pour tous $n,m\in\N$, $\sigma(n,m)$ désigne toujours un nombre \textbf{fini}.
\item Expliquer pourquoi si $H$ est calculable alors $\sigma$ l'est aussi.
\item Puisque l'on sait que toute machine peut-être simulée par une machine à seulement 2 symboles $\Sigma=\{1,B\}$, on fixera dans la suite $m=2$ et l'on notera $\sigma_2(n)=\sigma(n,2)$. Les entrées-sorties seront alors désormais supposées codées en unaire (par des blocs de $1$). Le résultat de la question~3 reste vrai si l'on remplace $\sigma$ par $\sigma_2$. Supposons $\sigma_2$ calculable et notons $\Sigma$ une machine la calculant (i.e. $\Sigma(1^n) = 1^{\sigma_2(n)}$).
\begin{enumerate}
\item Justifier proprement (en décrivant les grandes étapes du calcul) qu'il existe une machine $\Sigma'$ qui sur l'entrée $n$ (en unaire, i.e. $1^n$) s'arrête après au moins $\sigma_2(2n)+1$ étapes de calcul.\\
{\small\emph{Indication.}~~$\Sigma'$ pourra être décrite comme une simple composition de trois machines (dont la première multiplie l'entrée par 2 et la deuxième est $\Sigma$).}
\item Soit $k\in\N$ fixé. On note $E_k$ une machine qui, depuis l'entrée vide, s'arrête après avoir écrit $k$ en unaire (i.e. $1^k$) sur le ruban. Quel est le nombre d'états (en fonction de $k$) d'une telle machine $E_k$ ?
\item On note $K$ le nombre d'états de la machine $\Sigma'$. Quel est le nombre d'états de la machine composée $\Sigma'\circ E_K$ ? En combien d'étapes s'arrête-t-elle depuis l'entrée vide ?
\item Conclure à une absurdité et en déduire que $\sigma_2$ n'est pas calculable. En déduire le théorème de l'Arrêt (en appliquant une question précédente).
\end{enumerate}
\end{enumerate}


\exercice{Clôture vs. diagonalisation}{7 pts}
Soit $\F=\{f_0,f_1,f_2,\ldots\}$ un ensemble de fonctions \textbf{totales}
$f_i:\N\rightarrow\N$ qui est \emph{clos par addition} : pour tous indices $\alpha,\beta$, il
existe un indice $\gamma$ tel que, pour tout $x\in\N$,
$f_{\gamma}(x)=f_{\alpha}(x)+f_{\beta}(x)$ (additionner deux fonctions
de $\F$ produit encore une fonction de $\F$).
\begin{enumerate}
\item On suppose que $\F$ contient une fonction strictement positive
  en tout point : il existe un indice $\delta$ tel que pour tout
  $x\in\N$, $f_{\delta}(x)>0$. Montrer que la \emph{fonction diagonale}
  définie par $D(x)=f_x(x)$, pour tout $x\in\N$, n'est pas dans $\F$.
\item Justifier que l'on ne peut pas appliquer le résultat de la question~1 en
  choisissant $\F=\{\phi_0,\phi_1,\ldots\}$ l'énumération usuelle des
  fonctions calculables ($\phi_i$ la fonction calculée par la machine
  $M_i$ de code~$i$).
\item Pour chaque fonction calculable $\phi_i$ de l'énumération
    usuelle, on définit la fonction $\widetilde{\phi_i}$ par
    $\widetilde{\phi_i}(x)=0$ si $\phi_i(x)=\bot$ (i.e. n'est pas définie) et
    $\widetilde{\phi_i}(x)=\phi_i(x)$ sinon. Montrer que si l'on suppose l'Arrêt
    décidable, alors le résultat de la
    question~1 s'applique bien à
    $\F=\{\widetilde{\phi_0},\widetilde{\phi_1},\ldots\}$. Qu'en déduire pour $D$ ?\\
{\small\emph{Attention !}~~Toutes les fonctions de $\F$ ne sont pas
nécessairement calculables, en revanche toutes les fonctions
calculables totales sont dans $\F$.}
\item Montrer que si l'on suppose l'Arrêt décidable, alors $D$
    est calculable. Conclure.
\end{enumerate}

\groupe{3,14159265358979323846264338327950288419716939937510582097494459230781640628\ldots}

\exercice{Pi}{3 pts (of course)}
On définit la fonction $f:\N\rightarrow\N$ par $f(n)=1$ si le chiffre $3$ apparaît (au moins) $n$ fois consécutivement, c'est-à-dire qu'il apparaît un bloc $\underbrace{33\cdots3}_{(\geq)n\text{ fois}}$, dans les décimales du nombre $\pi=3,141592\ldots$ et $f(n)=0$ sinon.
\begin{enumerate}
\item Quelles valeurs de $f$ pouvez-vous déduire de l'approximation de $\pi$ donnée en tête de section ?
\item On ignore en réalité si des blocs arbitrairement grands de $3$ apparaissent dans les décimales de $\pi$\ldots
\begin{enumerate}
\item \ldots{}mais si c'est le cas, décrire les valeurs prises par la fonction $f$ ?
\item \ldots{}et si ce n'est pas le cas, que peut-on en conclure sur la taille des blocs de $3$ ? Décrire alors les valeurs prises par la fonction $f$.
\item La fonction $f$ est-elle calculable ?
\end{enumerate}
\item Il se trouve que les décimales de $\pi$ sont \emph{calculables} : il existe une machine qui prenant en entrée $n$ renvoie la $n$\ieme{} décimale de $\pi$. Cela joue-t-il implicitement un rôle dans votre raisonnement précédent ?
\end{enumerate}

%%%%% FIN %%%%%
\label{lastpage}
\end{document}
